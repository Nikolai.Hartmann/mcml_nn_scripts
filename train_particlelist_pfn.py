#!/usr/bin/env python

import numpy as np
import keras
from models import create_custom_pfn
import glob
from generate_batches_pfn import ParticleListsSequence, get_pdg_mapping


def set_gpu():
    import tensorflow as tf
    import keras.backend as K
    import os

    # Set GPU memory usage to only what's needed
    os.environ["CUDA_VISIBLE_DEVICES"] = "0"
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    config.gpu_options.visible_device_list = "0"
    session = tf.Session(config=config)
    K.set_session(session)


set_gpu()

model = create_custom_pfn()
model.compile(
    loss="binary_crossentropy",
    optimizer=keras.optimizers.Adam(
        #lr=0.00001 # try default first
    )
)

# batch generator
batch_files = glob.glob(
    "/srv/data/nhartmann/atlas_mcml_studies/dataframes_particlelists/even_batches_pfn/*.npy"
)

# 20% validation data
split_at = int(len(batch_files) * 0.8)

batch_files_train = batch_files[:split_at]
batch_files_val = batch_files[split_at:]

generator_opts = dict(use_class_weights=True, clip_weights=[-100, 100], scale_max_energy=True)

generator_train = ParticleListsSequence(batch_files_train, **generator_opts)
generator_val = ParticleListsSequence(batch_files_val, **generator_opts)

model.save("model_initial.h5")

model.fit_generator(
    generator_train,
    epochs=100,
    validation_data=generator_val,

    # potentially experiment with the following parameters
    # they might make it faster, although not much since probably i/o limited
    #workers=3,
    #use_multiprocessing=True,
    #max_queue_size=50,

    callbacks=[
        keras.callbacks.ModelCheckpoint("model_trained.h5", save_best_only=True),
        keras.callbacks.EarlyStopping(patience=5),
        keras.callbacks.CSVLogger("history.csv"),
    ]
)
