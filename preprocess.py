#!/usr/bin/env python

import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
import json


class Loader:

    def __init__(self, train_input):
        self.train_input = train_input
        self.x = None
        self.y = None
        self.w = None
        self.shift = None
        self.scale = None


    def read_h5(self, h5file, label=1):
        for df in pd.read_hdf(
                h5file,
                chunksize=100000,
        ):
            x_chunk = df[self.train_input].values
            y_chunk = np.ones(len(x_chunk))*label
            # hard code for now
            w_chunk = df["norm_weight"]*df["event_weight"]
            if self.x is None:
                self.x = x_chunk
                self.y = y_chunk
                self.w = w_chunk
            else:
                self.x = np.concatenate([self.x, x_chunk])
                self.y = np.concatenate([self.y, y_chunk])
                self.w = np.concatenate([self.w, w_chunk])


    def fit(self):
        # determine transformation parameters
        # for now, simply mean and std
        self.shift = self.x.mean(axis=0)
        self.scale = self.x.std(axis=0)

        # apply class weights, such that the sum of weights
        # for both classes is the same
        sumw_1 = self.w[self.y==1].sum()
        sumw_0 = self.w[self.y==0].sum()
        self.class_weights = np.array(
            [
                (sumw_1+sumw_0)/(2*sumw_0),
                (sumw_1+sumw_0)/(2*sumw_1)
            ]
        )


    def save_params(self, json_file):
        with open(json_file, "w") as f:
            params = {
                "shift" : list(self.shift.astype(float)),
                "scale" : list(self.scale.astype(float)),
                "class_weights" : list(self.class_weights.astype(float))
            }
            json.dump(params, f)


    def load_params(self, json_file):
        with open(json_file) as f:
            params = json.load(f)
        self.shift = np.array(params["shift"])
        self.scale = np.array(params["scale"])
        self.class_weights = np.array(params["class_weights"])


    def transform(self):
        # transform numbers to be in a small range around 0
        self.x -= self.shift
        self.x /= self.scale
        # float32 more than sufficient now
        if self.x.dtype != np.float32:
            self.x = self.x.astype(np.float32)


        self.w *= self.class_weights[self.y.astype(int)]
        # mean weight should be 1
        self.w /= self.w.mean()


if __name__ == "__main__":

    import os

    train_input = ["mt", "met_pt", "met_phi", "lep1_pt", "lep1_eta", "lep1_phi"]

    r = Loader(train_input)
    r.read_h5("/srv/data/nhartmann/atlas_mcml_studies/ntuple_truthdaod_pass.h5", 1)
    r.read_h5("/srv/data/nhartmann/atlas_mcml_studies/ntuple_truthdaod_rnd_fail.h5", 0)

    r.fit()
    r.save_params("transform_params.json")

    r.transform()

    # split into train/test and save
    for name, data in zip(
            ["x_train", "x_test", "y_train", "y_test", "w_train", "w_test"],
            train_test_split(r.x, r.y, r.w, test_size=0.5, shuffle=True, random_state=1234)
    ):
        np.save(name+".npy", data)
