#!/usr/bin/env python

import numpy as np
import os
from keras.utils import Sequence


def generate_batches(
    ar, out_dir="batches", batch_size=1000, block_size=10000, nsplit=10
):
    """
    Generate the batches (one file per batch) for the generator
    ar: the array/memmap to split into batches
    batch_size: size of each batch
    block_size: blocks to read from array (should be multiple of batch_size)
    nsplit: split into this number fractions of the whole array (to avoid filling the memory)
    """
    if not os.path.exists(out_dir):
        os.mkdir(out_dir)
    batch_i = 0
    for split_i, block_starts in enumerate(
        np.array_split(np.random.permutation(np.arange(0, len(ar), block_size)), nsplit)
    ):
        print(f"loading superblock {split_i}/{nsplit}")
        ar_superblock = []
        for block_start in block_starts:
            print(block_start)
            ar_superblock.append(ar[block_start : block_start + block_size])
        print("concatenate")
        ar_superblock = np.concatenate(ar_superblock)
        print("shuffle")
        np.random.shuffle(ar_superblock)
        for start in range(0, len(ar_superblock), batch_size):
            print(f"Saving batch {batch_i}")
            batch = ar_superblock[start : start + batch_size]
            np.save(os.path.join(out_dir, f"batch_{batch_i}.npy"), batch)
            batch_i += 1


def get_pdg_mapping():
    pdgids = np.loadtxt("pdgids.txt", dtype=np.int)
    pdg_mapping = {pdgid: i for i, pdgid in enumerate(pdgids)}
    return pdg_mapping

def map_pdg_id(x_pdg, pdg_dict):
        """
        map the pdg ids
        use trick (https://stackoverflow.com/a/16993364) to only look up each pdgid once in the dictionary
        """
        u, inv = np.unique(x_pdg, return_inverse=True)
        return np.array([pdg_dict[x] for x in u])[inv].reshape(x_pdg.shape)


class ParticleListsSequence(Sequence):

    """
    Generate batches from batch files
    """

    # hard coded for now - generate with `get_transform_params`
    transform_params = {
        "mean": np.array(
            [-1.9987043e-02, 3.1097829e-03, 3.4964830e-02, 3.2456545e04],
            dtype=np.float32,
        ),
        "std": np.array([6412.741, 6302.7026, 150451.27, 147280.98], dtype=np.float32),
        "weight_mean": 0.011815744,
        "class_weights": [0.5000065767001926, 38013.48412299094],
    }

    def __init__(
        self,
        batch_files,
        use_class_weights=True,
        clip_weights=None,
        scale_max_energy=False,
    ):

        self.batch_files = batch_files
        self.use_class_weights = use_class_weights
        self.clip_weights = clip_weights
        self.scale_max_energy = scale_max_energy

        self.pdg_dict = get_pdg_mapping()

    def __len__(self):
        return len(self.batch_files)



    def get_batch(self, idx, do_transform=True):
        batch_array = np.load(self.batch_files[idx])
        x_pdg = batch_array[:, :, 0]
        x_var = batch_array[:, :, 1:5]
        labels = batch_array[:, 0, 6]
        weights = batch_array[:, 0, 5]

        if do_transform:
            if self.scale_max_energy:
                # divide by energy of leading particle
                x_var /= np.max(x_var[:,:,3], axis=1).reshape(-1, 1, 1)
            else:
                for i in range(x_var.shape[-1]):
                    mask = x_var[:, :, i] != 0
                    x_var[:, :, i][mask] -= self.transform_params["mean"][i]
                    x_var[:, :, i][mask] /= self.transform_params["std"][i]
            weights /= self.transform_params["weight_mean"]
            if self.use_class_weights:
                weights *= np.array(self.transform_params["class_weights"])[
                    labels.astype(np.int)
                ]

        if self.clip_weights is not None:
            weights = np.clip(weights, *self.clip_weights)

        # map the pdgids here for now ... probably would be better to do that
        # in an earlier stage since it is rather slow compared to the other
        # operations
        x_pdg = map_pdg_id(x_pdg, self.pdg_dict)

        return [x_var, x_pdg], labels, weights

    def __getitem__(self, idx):
        return self.get_batch(idx)

    def get_transform_params(self, nbatches=100):
        mean = []
        std = []
        weight_mean = []
        sumw_0 = 0
        sumw_1 = 0
        for batch_idx in np.random.permutation(len(self))[:nbatches]:
            (var, pdg), y, w = self.get_batch(batch_idx, do_transform=False)
            # calculate mean/std only for non-zero values
            var[var == 0] = np.nan
            mean.append(np.nanmean(var, axis=(0, 1)))
            std.append(np.nanstd(var, axis=(0, 1)))
            weight_mean.append(w.mean())
            sumw_0 += w[y == 0].sum()
            sumw_1 += w[y == 1].sum()
        return {
            "mean": np.mean(mean, axis=0),
            "std": np.mean(std, axis=0),
            "weight_mean": np.mean(weight_mean),
            "class_weights": [
                (sumw_1 + sumw_0) / (2 * sumw_0),
                (sumw_1 + sumw_0) / (2 * sumw_1),
            ],
        }


if __name__ == "__main__":

    m = np.memmap(
        "/srv/data/nhartmann/atlas_mcml_studies/dataframes_particlelists/even_memmap_size1000_status1.bin",
        mode="r",
        dtype=np.float32,
    )
    evts = m.reshape(
        -1, 1000, 7
    )  # maxsize 1000 and ["pdg_id", "px", "py", "pz", "e", "weight", "label"] per particle

    generate_batches(
        evts,
        out_dir="/srv/data/nhartmann/atlas_mcml_studies/dataframes_particlelists/even_batches_pfn",
    )
