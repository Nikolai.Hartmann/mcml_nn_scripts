#!/usr/bin/env python

import logging
from multiprocessing import Process, Queue, Lock, Event, current_process
import queue

import numpy as np
import pandas as pd
import json

logger = logging.getLogger("batchgen_particlelist")

class InputConfiguration:

    # columns to be used for training
    # (except pdg id)
    columns = [
        'production_vertex',
        'end_vertex',
        'px',
        'py',
        'pz',
        'e',
        'status_1',
        'status_2',
        'status_3',
        'status_4',
        'status_11'
    ]

    # columns to be scaled and shifted
    transform_columns = [
        'production_vertex',
        'end_vertex',
        'px',
        'py',
        'pz',
        'e',
    ]

    # columns to be multiplied for the total weights
    weight_columns = [
        'event_weight',
        'norm_weight',
    ]

    # columns that uniquely identify one event
    id_columns = [
        'event_number',
        'dsid',
    ]

    # number of particles per event
    event_size = 2000

    # transform parameters (load later)
    # TODO: investigate if it is better to
    # scale not only column wise, but also row wise
    transform_scales = None
    transform_shifts = None

    # remapping for pdg ids
    # (load later)
    pdg_tokens_dict = {}

    # transform parameters for the weights
    class_weights = None
    mean_weight = None


    @property
    def shape_dict(self):
        return {
            "pdg_input" : [self.event_size],
            "particle_input" : [self.event_size, len(self.columns)],
        }


    @property
    def num_pdg_codes(self):
        return len(self.pdg_tokens_dict)+1


def put_chunks(label_0_list, label_1_list, nchunks, chunk_queue, exit_event):
    "continuosly put chunks into the chunk queue"
    # Loop over epochs
    while not exit_event.is_set():
        logger.debug("Reshuffle indices for loading full dataset")
        idx_0 = np.random.permutation(len(label_0_list))
        idx_1 = np.random.permutation(len(label_1_list))

        # Loop over chunks to be loadad into memory
        for idx_0_chunk, idx_1_chunk in zip(
                np.array_split(idx_0, nchunks),
                np.array_split(idx_1, nchunks)
        ):
            while not exit_event.is_set():
                try:
                    chunk_queue.put(
                        (label_0_list, label_1_list, idx_0_chunk, idx_1_chunk), timeout=1
                    )
                except queue.Full:
                    logger.debug("Chunk queue full, try again")
                    continue
                logger.debug("new chunk in the queue")
                break


class Worker(Process):

    def __init__(
        self,
        batch_queue,
        chunk_queue,
        exit_event,
        batch_size,
        event_size,
        input_config,
        io_lock=None,
        force_equal_batchsize=True,
        track_id_columns=False,
    ):

        super(Worker, self).__init__()

        self.batch_queue = batch_queue
        self.chunk_queue = chunk_queue
        self.exit_event = exit_event

        self.batch_size = batch_size
        self.event_size = event_size
        self.input_config = input_config

        self.io_lock = io_lock
        self.force_equal_batchsize = force_equal_batchsize
        self.track_id_columns = track_id_columns


    def run(self):
        while not self.exit_event.is_set():

            # retrieve a list of files and their labels from queue
            try:
                label_0_list, label_1_list, idx_0_chunk, idx_1_chunk = self.chunk_queue.get(timeout=1)
            except queue.Empty:
                logger.debug("No more chunks, try again")
                continue

            # if io_lock is set, let only one process read from disk at once
            # to achieve parallel i/o and batch generation
            if self.io_lock is not None:
                self.io_lock.acquire()
            try:
                logger.debug("Loading the next chunk into memory")
                dfs = []
                for i in idx_0_chunk:
                    logger.debug("reading {} for label 0".format(label_0_list[i]))
                    df = pd.read_hdf(label_0_list[i])
                    df["label"] = 0
                    dfs.append(df)
                for i in idx_1_chunk:
                    logger.debug("reading {} for label 1".format(label_1_list[i]))
                    df = pd.read_hdf(label_1_list[i])
                    df["label"] = 1
                    dfs.append(df)
                df = pd.concat(dfs)
            finally:
                if self.io_lock is not None:
                    self.io_lock.release()

            # transform inputs
            df[self.input_config.transform_columns] -= self.input_config.transform_shifts
            df[self.input_config.transform_columns] /= self.input_config.transform_scales

            # group the particles by event number
            # TODO: check if we can be more general and group by dsid+event_number
            # without loosing too much performance
            logger.debug("Generating batches")
            gb = df.groupby(self.input_config.id_columns)
            group_keys = list(gb.groups.keys())

            # shufffle events
            rnd_groups = np.random.permutation(len(group_keys))

            # generate batches from the current chunk
            for start in range(0, len(rnd_groups), self.batch_size):

                batch_group_idx = rnd_groups[start:start+self.batch_size]

                # last batch typically too small, skip if `force_equal_batchsize` requested
                if len(batch_group_idx) < self.batch_size and self.force_equal_batchsize:
                    continue

                # actual size of this batch (might be smaller if `force_equal_batchsize` not requested)
                curr_batch_size = len(batch_group_idx)

                # initialize np arrays of batch
                batch_x = {}
                batch_x["particle_input"] = np.zeros(
                    shape=(curr_batch_size, self.event_size, len(self.input_config.columns)), dtype=np.float32
                )
                batch_x["pdg_input"] = np.zeros(
                    shape=(curr_batch_size, self.event_size), dtype=np.int32
                )
                batch_y = np.zeros(shape=(curr_batch_size,), dtype=np.bool)
                batch_w = np.zeros(shape=(curr_batch_size,), dtype=np.float32)

                batch_id_columns_dfs = []

                # loop over events in batch and fill arrays
                for event_id, group_id in enumerate(batch_group_idx):

                    if self.exit_event.is_set():
                        return

                    df_event = gb.get_group(group_keys[group_id])
                    evt_x = df_event[self.input_config.columns].values

                    # index of last particle, rest will be filled with 0s
                    # (or cut off if longer than event_size)
                    last = min(self.event_size, len(evt_x))

                    batch_x["particle_input"][event_id][:last] = evt_x[:last]

                    # map pdg_ids to tokens in a fixed range
                    # (needed for embedding)
                    batch_x["pdg_input"][event_id][:last] = np.array(
                        [
                            self.input_config.pdg_tokens_dict[pdg_id]
                            if pdg_id in self.input_config.pdg_tokens_dict
                            else 0
                            for pdg_id in df_event["pdg_id"][:last]
                        ]
                    )

                    event_label = df_event.iloc[0]["label"]
                    batch_y[event_id] = event_label

                    # multipy all event weights, scale by class weight and divide by global mean
                    event_weight = None
                    for weight_column in self.input_config.weight_columns:
                        column_weight = df_event.iloc[0][weight_column]
                        if event_weight is None:
                            event_weight = column_weight
                        else:
                            event_weight *= column_weight
                    batch_w[event_id] = (
                        event_weight
                        * self.input_config.class_weights[int(event_label)]
                        / self.input_config.mean_weight
                    )

                    if self.track_id_columns:
                        batch_id_columns_dfs.append(df_event[self.input_config.id_columns][0:1])

                # send batch to queue
                while not self.exit_event.is_set():
                    try:
                        if not self.track_id_columns:
                            next_item = (batch_x, batch_y, batch_w)
                        else:
                            next_item = (batch_x, batch_y, batch_w, pd.concat(batch_id_columns_dfs))
                        self.batch_queue.put(next_item, timeout=1)
                    except queue.Full:
                        logger.debug("Batch queue is full, try again")
                        continue
                    break
                else:
                    logger.debug("Exit event is set - returning from {}".format(current_process()))
                    return


class DataLoader:

    def __init__(self, batch_queue_size=500, chunk_queue_size=20):
        self.workers = []
        self.batch_queue = Queue(batch_queue_size)
        self.chunk_queue = Queue(chunk_queue_size)
        self.exit_event = Event()


    def generator(
        self,
        label_0_list,
        label_1_list,
        input_config,
        nchunks=10,
        batch_size=128,
        seed=1234,
        nworkers=2,
        do_io_lock=True,
        force_equal_batchsize=True,
        callback=None,
        track_id_columns=False,
    ):
        """
        :param nchunks: number of chunks in which the data is load into memory. A
                        lower number means higher memory usage.

        :param batch_size: number of events in each batch

        :param event_size: number of rows in each event (rest filled with 0 or cut away if more)

        :param nworkers: number of processes to be spawned for generating the batches.

        :param do_io_lock: set a lock when reading in chunks - this can help when
                           using a lower number workers to ensure concurrent batch generation and i/o.
                           For larger number of threads it might be beneficial not to lock.

        :param callback: call this function (batch tuple as argument) for each batch.
        """

        # spawn workers
        batch_queue = self.batch_queue
        chunk_queue = self.chunk_queue
        io_lock = Lock() if do_io_lock else None
        exit_event = self.exit_event
        for iworker in range(nworkers):
            w = Worker(
                batch_queue, chunk_queue, exit_event,
                batch_size, input_config.event_size, input_config,
                io_lock, force_equal_batchsize, track_id_columns
            )
            w.daemon = True
            w.start()
            self.workers.append(w)

        np.random.seed(seed)

        label_0_list = np.array(label_0_list)
        label_1_list = np.array(label_1_list)

        chunk_worker = Process(
            target=put_chunks,
            args=(label_0_list, label_1_list, nchunks, chunk_queue, exit_event)
        )
        chunk_worker.daemon = True
        chunk_worker.start()
        self.workers.append(chunk_worker)

        # now we generate the batches from the queue where the workers put them
        while True:
            try:
                batch = batch_queue.get(timeout=1)
                if callback is not None:
                    callback(batch)
                yield batch[:3]
            except queue.Empty:
                logger.debug("No batch there yet, try again")


    def drain(self):
        """
        Drain the queues (remove all elements).
        This is nescessary to let all processes finish.
        """
        self.exit_event.set()
        for q in [self.chunk_queue, self.batch_queue]:
            while True:
                try:
                    q.get(timeout=1)
                except queue.Empty:
                    logger.debug("drained!")
                    break


def get_input_configuration(pdg_ids_json, transform_params_h5, weight_params_json):
    input_config = InputConfiguration()

    with open(pdg_ids_json) as f:
        json_dict = json.load(f)
        for k, v in json_dict["pdg_tokens_dict"].items():
            input_config.pdg_tokens_dict[int(k)] = int(v)

    input_config.transform_scales = pd.read_hdf(transform_params_h5, "scales")
    input_config.transform_shifts = pd.read_hdf(transform_params_h5, "shifts")

    with open(weight_params_json) as f:
        json_dict = json.load(f)
        input_config.class_weights = np.array(json_dict["class_weights"])
        input_config.mean_weight = json_dict["mean_weight"]

    return input_config


if __name__ == "__main__":

    import glob
    import os
    import sys
    from tqdm import tqdm
    try:
        from itertools import izip
    except ImportError:
        izip = zip

    import logging
    from time import sleep
    import matplotlib.pyplot as plt

    logging.basicConfig(
        level=logging.DEBUG,
    )

    #base_path = "/project/etp3/nhartmann/collect/wjets_mcdump_met150mt150_dataframes_particlelists/even"
    #base_path = "/srv/data/nhartmann/atlas_mcml_studies/dataframes_particlelists/even"
    base_path = sys.argv[1]

    label_0_list = sorted(list(glob.glob(os.path.join(base_path, "fail_*.h5"))))
    label_1_list = sorted(list(glob.glob(os.path.join(base_path, "pass_*.h5"))))

    input_config = get_input_configuration("pdg_ids.json", "transform_params.h5", "weight_params.json")

    def get_y_callback(batches_y, id_dfs):
        "Generate a callback function that appends the batch labels to a list"

        def callback(batch):
            batches_y.append(batch[1])
            id_dfs.append(batch[3])

        return callback

    batches_y = []
    id_dfs = []

    loader = DataLoader()
    generator = loader.generator(
        label_0_list, label_1_list,
        input_config,
        nchunks=min(len(label_1_list), len(label_0_list)),
        nworkers=1,
        do_io_lock=False,
        callback=get_y_callback(batches_y, id_dfs),
        track_id_columns=True
    )
    np.set_printoptions(linewidth=200, edgeitems=10)
    batch_x, batch_y, batch_w = next(generator)
    # test a longer loop over the generator
    for i, batch in tqdm(izip(range(100), generator)):
        pass
    loader.drain()

    labels = np.concatenate(batches_y)

    df_id = pd.concat(id_dfs)
    df_id.to_hdf("id_columns.h5", "id_columns")

    fig, ax = plt.subplots()
    ax.hist(batch_w, bins=100, range=(-3, 3))
    ax.set_yscale("log")
    fig.savefig("weights.pdf")

    merged_evt = np.concatenate([ar for ar in batch_x["particle_input"]])

    # validate the scaling for one batch
    fig, axs = plt.subplots(figsize=(20, 15), nrows=2, ncols=3)
    for i, (feature, ax) in enumerate(zip(input_config.transform_columns, axs.reshape(-1))):
        ar = merged_evt[:,i]
        ax.hist(ar[ar!=0], bins=100, range=(-3, 3))
        ax.set_xlabel(feature)
    fig.savefig("test.pdf")
