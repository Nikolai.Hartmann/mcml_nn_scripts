#!/usr/bin/env python

from batchgen_particlelist import InputConfiguration as Inputs

import pandas as pd
import glob
import os
import json
import numpy as np
from tqdm import tqdm
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("base_path")
parser.add_argument("--nfiles", default=20, type=int)
args = parser.parse_args()

#base_path = "/srv/data/nhartmann/atlas_mcml_studies/dataframes_particlelists/even"
base_path = args.base_path

file_list = np.array(list(glob.glob(os.path.join(base_path, "fail_*.h5"))) + list(glob.glob(os.path.join(base_path, "pass_*.h5"))))

inputs = Inputs() # take hardcoded default values

nfiles = args.nfiles

np.random.seed(1234)
rnd_ind = np.random.permutation(len(file_list))

dfs = []
for file_path in file_list[rnd_ind[:nfiles]]:
    print("reading", file_path)
    dfs.append(pd.read_hdf(file_path, columns=inputs.transform_columns))
df = pd.concat(dfs)
mean = df.mean()
std = df.std()
mean.to_hdf("transform_params.h5", "shifts")
std.to_hdf("transform_params.h5", "scales")
print(mean)
print(std)
