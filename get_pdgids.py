#!/usr/bin/env python

import pandas as pd
import numpy as np
import os
import glob
import json
import sys

#base_path = "/project/etp3/nhartmann/collect/wjets_mcdump_met150mt150_dataframes_particlelists/even"
#base_path = "/srv/data/nhartmann/atlas_mcml_studies/dataframes_particlelists/even"
base_path = sys.argv[1]

pdgids = set()

for h5file in glob.glob(os.path.join(base_path, "*.h5")):
    df = pd.read_hdf(h5file, columns=["pdg_id"])
    for pdg_id in np.unique(df["pdg_id"]):
        pdgids.add(pdg_id)
    print(h5file)

pdg_tokens_dict = {}
pdg_tokens_dict_inverse = {}
for index, pdg_id in enumerate(sorted(pdgids, key=lambda x : abs(x)), start=1):
    pdg_tokens_dict[int(pdg_id)] = int(index)
    pdg_tokens_dict_inverse[int(index)] = int(pdg_id)

print(pdg_tokens_dict)
print(pdg_tokens_dict_inverse)

with open("pdg_ids.json", "w") as of:
    json.dump({"pdg_tokens_dict" : pdg_tokens_dict, "pdg_tokens_dict_inverse" : pdg_tokens_dict_inverse}, of)
