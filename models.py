import keras
import keras.backend as K
from keras.layers import Input, Dense, TimeDistributed, dot, concatenate, Lambda, Embedding

def create_custom_pfn(
    particle_shape=(1000, 4), embedding_dim=8, n_pdg=552, phi_sizes=[100, 100, 128], F_sizes=[100, 100, 100],
    kernel_initializer="glorot_uniform", # this is keras default and appearantly better that EFN default (he uniform)
):
    Mask = Lambda(lambda X :  K.cast(K.any(K.not_equal(X, 0.), axis=-1), K.dtype(X)), name="mask")
    input_particles = Input(particle_shape, name="input_particles")
    input_pdg = Input((particle_shape[0],), name="input_pdg")
    embedding = Embedding(n_pdg, embedding_dim, input_length=particle_shape[0])(input_pdg)
    concat = concatenate([input_particles, embedding])
    layer = concat
    for phi_size in phi_sizes:
        layer = TimeDistributed(Dense(phi_size, activation="relu", kernel_initializer=kernel_initializer))(layer)
    mask = Mask(input_particles)
    phi_sum = dot(
        [layer, mask],
        axes=1,
        #normalize=True # this might also help ...
    )
    layer = phi_sum
    for F_size in F_sizes:
        layer = Dense(F_size, activation="relu", kernel_initializer=kernel_initializer)(layer)
    output = Dense(1, activation="sigmoid", kernel_initializer=kernel_initializer)(layer)
    return keras.models.Model(inputs=[input_particles, input_pdg], outputs=[output])
