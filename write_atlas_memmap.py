#!/usr/bin/env python

import pandas as pd
import numpy as np
from tqdm import tqdm
import glob


class Settings:
    columns = ["pdg_id", "px", "py", "pz", "e", "weight", "label"]
    maxsize = 1000


def get_const_event_size_array(df, settings, label=0):
    """
    Group particles in Dataframe per event and create array of constant size,
    cropped and padded with 0 to settings.maxsize
    """

    columns = settings.columns
    maxsize = settings.maxsize

    # sort, needed for grouping later
    df_sorted = df.set_index(["dsid", "event_number"]).sort_index().reset_index()

    # add total weight column
    df_sorted["weight"] = df_sorted.event_weight * df_sorted.norm_weight

    # add label (assume we have a unique label per dataframe)
    df_sorted["label"] = label

    # read from these arrays
    event_numbers = df_sorted.event_number.to_numpy()
    df_array = df_sorted[columns].to_numpy(dtype=np.float32)

    # and fill into this
    nevents = df_sorted.groupby(["dsid", "event_number"]).ngroups
    array = np.zeros((nevents, maxsize, len(columns)), dtype=np.float32)

    # do a manual "group by" (better performance)

    # first, loop through dsids
    dsids_unique, counts_dsid = np.unique(df_sorted.dsid, return_counts=True)
    start_dsid = 0
    ievent = 0
    for stop_dsid in np.cumsum(counts_dsid):
        df_array_dsid = df_array[start_dsid:stop_dsid]
        event_numbers_dsid = event_numbers[start_dsid:stop_dsid]
        start_dsid = stop_dsid
        # now, loop through event numbers
        event_numbers_unique, counts_event_numbers = np.unique(
            event_numbers_dsid, return_counts=True
        )
        start_event_number = 0
        for stop_event_number in np.cumsum(counts_event_numbers):
            nparticles = min(stop_event_number - start_event_number, maxsize)
            array[ievent, :nparticles, :] = df_array_dsid[
                start_event_number:stop_event_number
            ][:nparticles, :]
            start_event_number = stop_event_number
            ievent += 1

    return array


def particle_selection(df):
    # only use status 1 (final state) particles
    return df[df.status_1 == 1]


def write_memmap(
    input_paths, labels, memmap_path, settings, particle_selection=particle_selection
):
    """
    Write 0-padded events from all DataFrames (`input_paths`) to memmap
    (`memmap_path`). Set the label to the values in `labels`.
    """

    # first dimension is dummy value, to be extended
    shape = [10, settings.maxsize, len(settings.columns)]
    memmap = np.memmap(memmap_path, dtype=np.float32, shape=tuple(shape), mode="w+")
    start = 0
    for label, df_path in tqdm(zip(labels, input_paths)):
        df = pd.read_hdf(df_path)
        df = particle_selection(df)
        array = get_const_event_size_array(df, settings, label)
        stop = start + len(array)
        if stop > len(memmap):
            # grow memmmap according to https://stackoverflow.com/questions/20932361/resizing-numpy-memmap-arrays
            memmap.flush()
            memmap = np.memmap(memmap_path, dtype=np.float32, shape=tuple(shape))
            memmap.base.resize(stop * shape[1] * shape[2] * 4)
            shape[0] = stop
            memmap.flush()
            memmap = np.memmap(memmap_path, dtype=np.float32, shape=tuple(shape))
        memmap[start:stop] = array
        start = stop


if __name__ == "__main__":

    import glob
    import os

    input_paths = glob.glob(
        "/srv/data/nhartmann/atlas_mcml_studies/dataframes_particlelists/even/*.h5"
    )
    labels = [0 if os.path.basename(i).startswith("fail") else 1 for i in input_paths]
    settings = Settings()

    write_memmap(
        input_paths,
        labels,
        f"/srv/data/nhartmann/atlas_mcml_studies/dataframes_particlelists/even_memmap_size{settings.maxsize}_status1.bin",
        settings
    )


