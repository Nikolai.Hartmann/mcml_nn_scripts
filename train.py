#!/usr/bin/env python

import os
import argparse

import numpy as np
import tensorflow as tf
import keras.backend as K
import keras
from keras.models import Sequential
from keras.layers import Dense, Activation, BatchNormalization
from keras.callbacks import CSVLogger, EarlyStopping, ModelCheckpoint

parser = argparse.ArgumentParser()
parser.add_argument("--gpu", action="store_true")
args = parser.parse_args()

if not args.gpu:
    print('Running on CPU only')
    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
    os.environ['OMP_NUM_THREADS'] = '1'
else:
    print('Running on GPU')
    # Set GPU memory usage to only what's needed
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    config.gpu_options.visible_device_list = "0"
    session = tf.Session(config=config)
    K.set_session(session)

x = np.load("x_train.npy")
y = np.load("y_train.npy")
w = np.load("w_train.npy")


# very simple 1-hidden-layer network
model = Sequential([
    Dense(100, input_dim=x.shape[1], activation="relu"),
    Dense(1, activation="sigmoid")
])

model.compile(optimizer='adam',
              loss='binary_crossentropy',
              weighted_metrics=['accuracy'])

callbacks = [
    CSVLogger('training.log'),
    EarlyStopping(patience=5, verbose=True),
    ModelCheckpoint(
        "weights.h5",
        save_best_only=True,
        verbose=True,
    ),
]

model.fit(
    x,
    y,
    sample_weight=w,
    epochs=100,
    batch_size=1000,
    validation_split=0.33,
    callbacks=callbacks
)
