#!/usr/bin/env python

"""
A little helper to get predictions of a neural network by first serializing
the input to /tmp, then running predict in a subprocess that saves the scores
and retrieving them back.

Why?
- Apparantly there is a problem with tf (or Nvidia) that the memory won't
be freed after having run a tensorflow session that allocated GPU memory

This assumes that the scores are always an np array, while the input data X may
be a dictionary of arrays.

The function will store the scores in the current directory - the filename
being a hash. This has the advantage that when run twice on the same data and
model it will not be evaluated again, but instead directly return the saved scores.

Usage:

>>> from eval_model_cached import predict_from_model
>>> scores = predict_from_model(model_h5, X)

"""

import os
import zlib
import hashlib
import multiprocessing
import warnings

import keras
from keras import backend as K
import tensorflow as tf
import numpy as np

def _np_adler32(X, start=1):
    if X.base is not X:
        warnings.warn(
            "will copy the array for calculating the hash, "
            "since it seems to be a view",
            RuntimeWarning
        )
        return zlib.adler32(np.array(X), start)
    else:
        return zlib.adler32(X, start)


def _save_scores(model_h5, x_npy, scores_file, is_dict=False):
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    config.gpu_options.visible_device_list = "0"
    session = tf.Session(config=config)
    K.set_session(session)
    if not is_dict:
        X = np.load(x_npy)
    else:
        import joblib
        X = joblib.load(x_npy)
    model = keras.models.load_model(model_h5)
    scores = model.predict(X, verbose=1, batch_size=1024)
    np.save(scores_file, scores)


def predict_from_model(model_h5, X):
    if not isinstance(X, dict):
        X_a32sum = _np_adler32(X)
    else:
        X_a32sum = 1
        for k, v in sorted(X.items(), key=lambda x : x[0]):
            X_a32sum = _np_adler32(v, X_a32sum)
    scores_hash = hashlib.sha1(
        "{}{}{}".format(model_h5, X_a32sum, os.path.getmtime(model_h5)).encode()
    ).hexdigest()
    scores_filename = "scores_{}.npy".format(scores_hash)
    if not os.path.exists(scores_filename):
        x_filename = "/tmp/x_{}.npy".format(X_a32sum)
        if isinstance(X, dict):
            import joblib
            joblib.dump(X, x_filename)
            is_dict = True
        else:
            np.save(x_filename, X)
            is_dict = False
        p = multiprocessing.Process(
            target=_save_scores, args=(model_h5, x_filename, scores_filename, is_dict)
        )
        p.start()
        p.join()
        os.unlink(x_filename)
    return np.load(scores_filename)
