#!/usr/bin/env python

import numpy as np
from tqdm import tqdm
import keras
import pandas as pd

def get_labels_and_weights(generator):
    labels = []
    weights = []
    print("Fetching labels and weights")
    for i in tqdm(range(len(generator))):
        labels.append(np.array(generator[i][1]))
        weights.append(np.array(generator[i][2]))
    return np.concatenate(labels), np.concatenate(weights)

def get_prediction_df(model, generator):
    y, w = get_labels_and_weights(generator)
    print("Run model")
    scores = model.predict_generator(generator, verbose=1)
    return pd.DataFrame(dict(y=y, w=w, scores=scores.ravel()))

if __name__ == "__main__":

    import glob
    import os
    import argparse

    import keras
    from energyflow.archs import PFN

    from generate_batches_pfn import ParticleListsSequence

    parser = argparse.ArgumentParser(
        description=(
            "Run prediction for trained model on batches from a directory. "
            "Store the corresponding scores, labels and weights into a DataFrame."
        )
    )
    parser.add_argument("model")
    parser.add_argument("batch_dir")
    parser.add_argument("output_df")
    args = parser.parse_args()

    generator = ParticleListsSequence(glob.glob(os.path.join(args.batch_dir, "*")))

    model = keras.models.load_model(args.model)

    df = get_prediction_df(model, generator)
    df.to_hdf(args.output_df, "result")
