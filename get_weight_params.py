#!/usr/bin/env python

from batchgen_particlelist import InputConfiguration as Inputs

import pandas as pd
import glob
import os
import json
import numpy as np
from tqdm import tqdm
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("base_path")
parser.add_argument("--nfiles", default=390, type=int)      #Anzahl der verwendeten Dateien 
parser.add_argument("--seed", default=1234, type=int)
args = parser.parse_args()

base_path = args.base_path

file_list_0 = np.array(list(glob.glob(os.path.join(base_path, "fail_*.h5"))))
file_list_1 = np.array(list(glob.glob(os.path.join(base_path, "pass_*.h5"))))

file_list = np.concatenate([file_list_0, file_list_1])

file_labels = np.concatenate([
    np.zeros(len(file_list_0)),
    np.ones(len(file_list_1))
])

inputs = Inputs() # take hardcoded default values

nfiles = args.nfiles

np.random.seed(args.seed)
rnd_ind = np.random.permutation(len(file_list))

dfs = []
for file_path, file_label in zip(file_list[rnd_ind[:nfiles]], file_labels[rnd_ind[:nfiles]]):
    print("reading", file_path)
    file_df = pd.read_hdf(file_path, columns=inputs.weight_columns+inputs.id_columns)
    file_df.drop_duplicates(subset=inputs.id_columns, inplace=True)
    file_df["label"] = file_label
    dfs.append(file_df[inputs.weight_columns+["label"]])
df = pd.concat(dfs)

total_weight = None
for weight_column in inputs.weight_columns:
    weight = df[weight_column]
    if total_weight is None:
        total_weight = weight
    else:
        total_weight *= weight

sumw_0 = total_weight[df["label"]==0].sum()
sumw_1 = total_weight[df["label"]==1].sum()
class_weights = [
    (sumw_1+sumw_0)/(2*sumw_0),
    (sumw_1+sumw_0)/(2*sumw_1)
]
mean_weight = total_weight.mean()

print("test:")

print(
    (total_weight * np.array(class_weights)[df["label"].values.astype(int)] / mean_weight).mean()
)

with open("weight_params.json", "w") as of:
    json.dump({"class_weights" : class_weights, "mean_weight" : mean_weight}, of)

