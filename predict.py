#!/usr/bin/env python

import keras
import keras.backend as K
from keras import *
import tensorflow as tf
import numpy as np
import pandas as pd
import argparse
import os
import sys
import glob
from tqdm import tqdm
try:
    from itertools import izip
except ImportError:
    izip = zip
from train_particlelist import generators
from batchgen_particlelist import get_input_configuration, DataLoader

parser = argparse.ArgumentParser()
parser.add_argument("base_path")
parser.add_argument("--cpu", action="store_true")
parser.add_argument("--validation_path")
args = parser.parse_args()

if args.cpu:
    print('Running on CPU only')
    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
    os.environ['OMP_NUM_THREADS'] = '1'
else:
    print('Running on GPU')
    # Set GPU memory usage to only what's needed
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    config.gpu_options.visible_device_list = "0"
    session = tf.Session(config=config)
    K.set_session(session)

input_config = get_input_configuration("pdg_ids.json", "transform_params.h5", "weight_params.json")

loader_train = DataLoader()
loader_test = DataLoader()

generator_train, generator_test, batches_train_y,  batches_test_y, batches_train_w, batches_test_w = generators(loader_train, loader_test, args, input_config)

for i, batch in tqdm(izip(range(1000), generator_train)):
    pass
for i, batch in tqdm(izip(range(1000), generator_test)):
    pass
    
labels_train = np.concatenate(batches_train_y)
labels_test  = np.concatenate(batches_test_y)

w_train = np.concatenate(batches_train_w)
w_test  = np.concatenate(batches_test_w)

modelname = str(input("Name of model?  "))
print('load model...')
model = models.load_model(modelname)

print('predicting train...')
scores_train = model.predict_generator(generator_train, steps=1000)
print('predicting test...')
scores_test = model.predict_generator(generator_test, steps=1000)

loader_train.drain()
loader_test.drain()

print('saving scores_train...')
np.save("scores_train_"+modelname+".npy", scores_train)
print(scores_train.shape)

print('saving scores_test...')
np.save("scores_test_"+modelname+".npy", scores_test)
print(scores_test.shape)

print('saving labels...')
np.save("labels_train_"+modelname+".npy", labels_train)
np.save("labels_test_"+modelname+".npy", labels_test)

print('saving weights...')
np.save("w_train_"+modelname+".npy", w_train)
np.save("w_test_"+modelname+".npy", w_test)