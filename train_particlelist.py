#!/usr/bin/env python

from smartBKG.train import NNBaseClass #von James

from keras.models import Model
from keras.callbacks import CSVLogger, EarlyStopping, ModelCheckpoint
from keras.layers import (
    Dense,
    Dropout,
    Input,
    LeakyReLU,
    Conv1D,
    GlobalAveragePooling1D,
    MaxPooling1D,
    AveragePooling1D,
    Embedding,
    BatchNormalization,
    concatenate,
    Add
)
from keras import optimizers
import tensorflow as tf
import keras.backend as K
import os
import glob
import numpy as np
from batchgen_particlelist import DataLoader

class CNNVanillaModel(NNBaseClass):
    """
    adapted from james example:
    examples/train/models/particles/CNN_vanilla_particles.py
    """

    def __init__(self, shape_dict, num_pdg_codes):

        # note: we use this a bit different here (in contrast to james examples)
        # - the batch size dimension is ommited!
        self.shape_dict = shape_dict

        self.num_pdg_codes = num_pdg_codes


    def build_model(self):
        # Create the embedding layer for the pdg input
        pdg_embedding = Embedding(
            self.num_pdg_codes,
            8, #auf 8 Dimensionen gemappt
            input_length=self.shape_dict['pdg_input'],
        )

        # Network to process individual particles
        particle_input = Input(shape=self.shape_dict['particle_input'], name='particle_input')

        # Embed PDG codes
        pdg_input = Input(shape=self.shape_dict['pdg_input'], name='pdg_input')

        pdg_l = pdg_embedding(pdg_input)

        # Combine particle input and pdg embedding
        particle_l = concatenate([particle_input, pdg_l], axis=-1)

        # conv1D_avg_node does 2 1D conv layers with BatchNormalization,
        # LeakyReLU and an optional pooling layer after the 2 conv layers
        particle_l = self.conv1D_avg_node(  #conv1d_avg_node von James: 2 hintereinander geschaltene 1D convolutions
            particle_l,
            filters=64,
            kernel_size=3,
            pool='avg',  #average-pooling
        )
        particle_l = self.conv1D_avg_node(
            particle_l,
            filters=64,
            kernel_size=3,
            # pool='avg',
        )
        particle_l = self.conv1D_avg_node(
            particle_l,
            filters=64,
            kernel_size=3,
            # pool='avg',
        )
        #=> 6 Convolutional layers

        # Flatten by GlobalAveragePooling1D (average each feature globally)
        particle_output = GlobalAveragePooling1D()(particle_l)
            #über jedes feature was da rauskommt (64 features),
            # 64 Listen, die genauso lang sind wie die ursprüngliche Liste (ursprünglich Anzahl columns (px, py, ...))
            # wird gemappt auf 64 (NN-artige), über 64 nur ein globaler average

        # Finally a Dense network
        dense_l = Dense(512)(particle_output) #64 wird hier reingesteckt, usw.
        dense_l = LeakyReLU()(dense_l)
        dense_l = Dropout(0.4)(dense_l)
        dense_l = Dense(128)(dense_l)
        dense_l = LeakyReLU()(dense_l)
        output = Dense(1, activation='sigmoid', name='y_output')(dense_l)

        # Instantiate the cnn model
        model = Model(
            inputs=[particle_input, pdg_input],
            outputs=output,
            name='particles-CNN-vanilla'
        )
        # Finally compile the model
        model.compile(
            loss='binary_crossentropy',
            optimizer="adam",
            metrics=['accuracy'],
        )

        with open('summary.txt','w') as fh:
            # Pass the file handle in as a lambda function to make it callable
            model.summary(print_fn=lambda x: fh.write(x + '\n'))
        model.summary()

        self.model = model


def get_y_callback(batches_y, batches_w):
        "Generate a callback function that appends the batch labels to a list"

        def callback(batch):
            batches_y.append(batch[1])
            batches_w.append(batch[2])

        return callback


def generators(loader_train, loader_val, args, input_config):
    base_path = args.base_path
    label_0_list_train = sorted(list(glob.glob(os.path.join(base_path, "fail_*.h5"))))
    label_1_list_train = sorted(list(glob.glob(os.path.join(base_path, "pass_*.h5"))))

    if args.validation_path is not None:
        label_0_list_val = sorted(list(glob.glob(os.path.join(args.validation_path, "fail_*.h5"))))
        label_1_list_val = sorted(list(glob.glob(os.path.join(args.validation_path, "pass_*.h5"))))
    else:
        label_0_list_val = label_0_list_train
        label_1_list_val = label_1_list_train

    batches_train_y=[]
    batches_val_y=[]
    batches_train_w=[]
    batches_val_w=[]

    #Generator for train:
    generator_train = loader_train.generator(
        label_0_list_train, label_1_list_train,
        input_config,
        nchunks=min(len(label_1_list_train), len(label_0_list_train)), #das kleinere der Länge von beiden Listen -> nur max. 1 file pro Chunck
        nworkers=10,
        seed=1234,
        do_io_lock=False,
        callback=get_y_callback(batches_train_y, batches_train_w)
    )

    #Generator for val:
    generator_val = loader_val.generator(
        label_0_list_val, label_1_list_val, input_config,
        nchunks=min(len(label_1_list_val), len(label_0_list_val)),
        nworkers=10,
        seed=1235,
        callback=get_y_callback(batches_val_y, batches_val_w)
    )

    return generator_train, generator_val, batches_train_y, batches_val_y, batches_train_w, batches_val_w


if __name__ == "__main__":

    import os
    import glob
    import sys
    import argparse
    from tqdm import tqdm
    try:
        from itertools import izip
    except ImportError:
        izip=zip
    from batchgen_particlelist import DataLoader, get_input_configuration, logger

    import logging
    logging.basicConfig()
    #logger.setLevel(logging.DEBUG)

    parser = argparse.ArgumentParser()
    parser.add_argument("base_path")
    parser.add_argument("--cpu", action="store_true")
    parser.add_argument("--validation_path")
    args = parser.parse_args()

    # this time we probably want gpu by default ;)
    if args.cpu:
        print('Running on CPU only')
        os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
        os.environ['OMP_NUM_THREADS'] = '1'
    else:
        print('Running on GPU')
        # Set GPU memory usage to only what's needed
        os.environ['CUDA_VISIBLE_DEVICES'] = '0'
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        config.gpu_options.visible_device_list = "0"
        session = tf.Session(config=config)
        K.set_session(session)

    input_config = get_input_configuration("pdg_ids.json", "transform_params.h5", "weight_params.json")

    loader_train = DataLoader()
    loader_val = DataLoader()

    generator_train, generator_val, *labels_weights = generators(
        loader_train, loader_val, args, input_config
    )

    m = CNNVanillaModel(
        shape_dict=input_config.shape_dict,
        num_pdg_codes=input_config.num_pdg_codes,
    )
    m.build_model()

    callbacks= [
        CSVLogger('training.log'),
        #EarlyStopping(patience=?, verbose=True),
        ModelCheckpoint(
            "weights40.{epoch:02d}-{val_loss:.4f}.h5",
            save_best_only=False,
            verbose=True
        )
    ]

    m.model.fit_generator(                      #statt fit, fit_generator
        generator_train,                        #statt x, y, w
        epochs=1,
        steps_per_epoch=100,                  #wie viele Batches sind eine Epoche (bei 1000 noch nicht ganzer Datensatz -> 128.000 statt 2.000.000)
                                                #nach 10 Epochen wärens 1 Epoche, wie schnell konvergierts? Evtl höher stellen...
        validation_data=generator_val,
        validation_steps=10,                  #nach jeder Epoche wird einmal validation gemacht (wie viele Batches vom generator_val.)
                                                #wenns zu sehr fluktuiert, vllt höher setzen
        #callbacks=callbacks
    )

    loader_train.drain()
    loader_val.drain()
